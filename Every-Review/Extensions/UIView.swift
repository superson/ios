//
//  UIViewExtension.swift
//  Every-Review
//
//  Created by 조수형 on 2019/10/12.
//  Copyright © 2019 suhyeongCho. All rights reserved.
//

import UIKit


extension UIView {
    
    func borderAll() {
        layer.borderWidth = 1.0
        layer.borderColor = tintColor.cgColor
    }
    
    
    func underlined() {
        let border = CALayer()
        let width: CGFloat = 0.5
        border.borderColor = UIColor.lightGray.cgColor
        border.frame = CGRect(x: 0, y: frame.size.height + 8, width: frame.size.width, height: width)
        border.borderWidth = width
        layer.addSublayer(border)
        layer.masksToBounds = true
    }
    
    func borderRounded(radius: CGFloat) {
        layer.cornerRadius = radius
        clipsToBounds = true
    }
}

