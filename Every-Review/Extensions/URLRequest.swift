//
//  URLRequest.swift
//  Every-Review
//
//  Created by 조수형 on 2019/10/12.
//  Copyright © 2019 suhyeongCho. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

enum Operation {
    case Signup, Login, Logout, Default
}


struct Resource<T> {
    let url: URL
}


extension URLRequest {
    static func load<T: Decodable>(resource: Resource<T>) -> Observable<T> {
        
        
        return Observable.just(resource.url)
            .flatMap { url -> Observable<Data> in
                var request = URLRequest(url: url)
                                
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                
                return URLSession.shared.rx.data(request: request)
        }.map { data -> T in
            
            return try JSONDecoder().decode(T.self, from: data)
        }.asObservable()
    }
    
    static func post<T>(resource: Resource<T>, body: [String: Any]? = nil, operation: Operation = .Default) -> Observable<(Int)> {
        
        return Observable.just(resource.url)
            .flatMap { url -> Observable<(response: HTTPURLResponse, data: Data)> in
                var request = URLRequest(url: url)
                
                request.httpMethod = "POST"
                
                if let body = body {
                    request.httpBody = try JSONSerialization.data(withJSONObject: body)
                }
                
                if operation == .Logout {
                    let cookies = HTTPCookieStorage.shared.cookies(for: url)!

                    let headers = HTTPCookie.requestHeaderFields(with: cookies)
                    
                    request.allHTTPHeaderFields = headers
                    request.setValue(cookies[0].value, forHTTPHeaderField: "csrf_token")
                } else if operation == .Login{
                    request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                    request.setValue("application/json", forHTTPHeaderField: "Accept")
                } else {
                    
                    let cookies = HTTPCookieStorage.shared.cookies(for: url)!

                    let headers = HTTPCookie.requestHeaderFields(with: cookies)
                    
                    request.allHTTPHeaderFields = headers
                    
                    request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                    request.setValue("application/json", forHTTPHeaderField: "Accept")
                }
                
                
                
                return URLSession.shared.rx.response(request: request)
        }.map { response, data -> Int in
            print(response.allHeaderFields)
            let statusCode = response.statusCode
            let url = response.url!
            print(String(data: data, encoding: .utf8)!)
            
            if statusCode == 200 {
                let headerField: [String: String]

                if operation == .Login {
                    headerField = response.allHeaderFields as! [String: String]
                    let cookies = HTTPCookie.cookies(withResponseHeaderFields: headerField, for: url)
                    HTTPCookieStorage.shared.setCookies(cookies, for: url, mainDocumentURL: nil)
                } else if operation == .Logout {
                    headerField = [:]
                    let cookies = HTTPCookie.cookies(withResponseHeaderFields: headerField, for: url)
                    HTTPCookieStorage.shared.setCookies(cookies, for: url, mainDocumentURL: nil)
                }
                print(HTTPCookieStorage.shared.cookies)
            }
            return statusCode
        }.asObservable()
    }
}
