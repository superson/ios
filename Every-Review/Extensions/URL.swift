//
//  URL.swift
//  Every-Review
//
//  Created by 조수형 on 2019/10/12.
//  Copyright © 2019 suhyeongCho. All rights reserved.
//

import Foundation


enum ReviewOrder {
    case recent, recommand, rank_high, rank_low
}

protocol API {
}

//class ArticleAPI {
//    let title: String
//
//    func reviewUrl() -> URL {
//        self.baseUrl.appendPathComponent("/review/pages/\(titleEncoding(title: title))/")
//    }
//}

extension URL {
    
    static var baseUrl = URL(string: "http://52.79.175.114:8000")!
    
    static let loginUrl = baseUrl.appendingPathComponent("/account/login/")
    
    static let logoutUrl = baseUrl.appendingPathComponent("/account/logout/")

    static let signupUrl = baseUrl.appendingPathComponent("/account/signup/")

    static let createPageUrl = baseUrl.appendingPathComponent("/wiki/create/")
    
    
    static func likeURL(id: String) -> URL {
        return baseUrl.appendingPathComponent("/review/like/\(id)/")
    }
    
    static func pageUrl(title: String, orderBy: ReviewOrder = .recent) -> URL {
        return baseUrl.appendingPathComponent("/wiki/pages/\(title)/")
    }
    
    static func searchUrl(title: String) -> URL {
        return URL(string: "http://52.79.175.114:8000/wiki/search?search_str=\(title.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)")!
    }
    
    static func reviewUrl(title: String, index: Int = 0) -> URL {
        return URL(string: "http://52.79.175.114:8000/review/pages/\(title.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)/?index=\(index)")!
    }
    
    static func editInfoUrl(title: String) -> URL {
        return pageUrl(title: title).appendingPathComponent("edit/")
    }
}
