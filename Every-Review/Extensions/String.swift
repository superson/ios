//
//  String.swift
//  Every-Review
//
//  Created by 조수형 on 2019/11/06.
//  Copyright © 2019 suhyeongCho. All rights reserved.
//

import Foundation

extension String {
    var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
    }
}
