//
//  Card.swift
//  Every-Review
//
//  Created by 조수형 on 2019/10/12.
//  Copyright © 2019 suhyeongCho. All rights reserved.
//

import UIKit

extension UICollectionViewCell {
    func makeCardViewCell() {
        self.borderRounded(radius: 6)

        self.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.16).cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.layer.shadowRadius = 4.0
        self.layer.shadowOpacity = 1.0
        self.layer.masksToBounds = false
        self.layer.shadowPath = UIBezierPath(rect: bounds).cgPath
    }
}
