//
//  FeedTableViewController.swift
//  Every-Review
//
//  Created by 조수형 on 2019/10/13.
//  Copyright © 2019 suhyeongCho. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class CreatePageViewController: UIViewController {
    @IBOutlet var titleTextField: UITextField!
    @IBOutlet var descriptionTextView: UITextView!
    
    let disposeBag = DisposeBag()
    
    @IBAction func createPageButtonClicked(_ sender: UIButton!) {

        if titleTextField.text == "" {
            let alert = UIAlertController(title: nil, message: "항목이 생성에 실패하였습니다".localized, preferredStyle: .alert)
            
            let ok = UIAlertAction(title: "확인".localized, style: .default, handler: nil)
            
            alert.addAction(ok)
            
            self.present(alert, animated: true)
            return
        }
        
        let resource = Resource<Basic>(url: URL.createPageUrl)
        
        let body: [String: Any] = ["title": titleTextField.text, "content" : descriptionTextView.text]

        URLRequest.post(resource: resource, body: body)
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { code in
                if code == 200 {
                    let alert = UIAlertController(title: nil, message: "항목이 생성되었습니다".localized, preferredStyle: .alert)
                    
                    let ok = UIAlertAction(title: "확인".localized, style: .default, handler: nil)
                    
                    alert.addAction(ok)
                    
                    self.present(alert, animated: true)
                } else {
                    let alert = UIAlertController(title: nil, message: "항목이 생성에 실패하였습니다".localized, preferredStyle: .alert)
                    
                    let ok = UIAlertAction(title: "확인".localized, style: .default, handler: nil)
                    
                    alert.addAction(ok)
                    
                    self.present(alert, animated: true)
                }
            }).disposed(by: disposeBag)
        
    }
}

extension CreatePageViewController: UITextFieldDelegate {
    override func touchesBegan(_: Set<UITouch>, with _: UIEvent?) {
        view.endEditing(true)
    }

    func textFieldShouldReturn(_: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
}
