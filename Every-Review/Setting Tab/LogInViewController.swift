//
//  SignInViewController.swift
//  Every-Review
//
//  Created by 조수형 on 18/07/2019.
//  Copyright © 2019 suhyeongCho. All rights reserved.
//

import UIKit
import RxSwift

class LogInViewController: UIViewController {
    
    
    @IBOutlet var loginIdTextField: UITextField!
    @IBOutlet var loginPasswordTextField: UITextField!
    
    
    private var loginSubject = PublishSubject<Bool>()
    
    var loginObserver: Observable<Bool> {
        return loginSubject.asObserver()
    }
    let disposeBag = DisposeBag()
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    @IBAction func loginButtonClicked(sender: UIButton!) {
        
        let resource = Resource<Basic>(url: URL.loginUrl)
        let body: [String: Any] = ["username": loginIdTextField.text,"password": loginPasswordTextField.text]
        
        let userid = loginIdTextField.text
        URLRequest.post(resource: resource, body: body, operation: .Login)
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { code in
                if code == 200 {
                    
                    
                    UserDefaults.standard.set(true, forKey: "UserLogin")
                    UserDefaults.standard.set(userid, forKey: "UserName")
                    
                    self.loginSubject.onNext(true)
                    
                    self.navigationController?.popViewController(animated: true)

                } else {
                    let alert = UIAlertController(title: "로그인 오류".localized, message: "아이디 또는 비밀번호가 일치하지 않습니다".localized, preferredStyle: .alert)
                    let cancel = UIAlertAction(title: "확인".localized, style: .cancel, handler: nil)
                    
                    alert.addAction(cancel)
                    self.present(alert, animated: true)
                }
            }).disposed(by: disposeBag)
        
    }
    

}
