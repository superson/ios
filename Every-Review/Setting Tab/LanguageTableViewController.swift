//
//  LanguageViewController.swift
//  Every-Review
//
//  Created by 조수형 on 2019/11/06.
//  Copyright © 2019 suhyeongCho. All rights reserved.
//

import UIKit

class LanguageTableViewController: UITableViewController {

    let languagesList = ["한국어", "English"]
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return languagesList.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LanguageCell", for: indexPath)
        
        cell.textLabel?.text = languagesList[indexPath.row]
        
        let code = UserDefaults.standard.array(forKey: "AppleLanguages")![0] as! String
        
        
        let language = changeCodeToLanguage(code: code)
        
        if language == languagesList[indexPath.row] {
            cell.accessoryType = .checkmark
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        let alert = UIAlertController(title: nil, message: "프로그램 재실행시부터\n선택하신 언어가 적용됩니다.".localized, preferredStyle: .alert)
        
        let ok = UIAlertAction(title: "확인".localized, style: .default) { _ in
            let language = self.languagesList[indexPath.row]
            let code = self.changeLanguageToCode(language: language)
            UserDefaults.standard.set([code], forKey: "AppleLanguages")
            
            self.navigationController?.popViewController(animated: true)
        }
        
        let cancel = UIAlertAction(title: "취소".localized, style: .cancel, handler: nil)
        
        
        alert.addAction(ok)
        alert.addAction(cancel)
        
        present(alert, animated: true)
    }
    
    func changeCodeToLanguage(code: String) -> String {
        let language: String!

        switch code {
            case "ko":
            language = "한국어"
        default:
            language = "English"
        }
        
        return language
    }
    
    func changeLanguageToCode(language: String) -> String {
        let code: String!
        
        switch language {
            
        case "한국어":
            code = "ko"
        default:
            code = "en"
        }
        
        
        return code
    }
}

