//
//  SignUpViewController.swift
//  Every-Review
//
//  Created by 조수형 on 2019/10/28.
//  Copyright © 2019 suhyeongCho. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class SignUpViewController: UIViewController {
    
    @IBOutlet var signupEmailTextField: UITextField!
    @IBOutlet var signupUsernameTextField: UITextField!
    @IBOutlet var signupPasswordTextField: UITextField!
    @IBOutlet var signupPasswordAgainTextField: UITextField!

    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func signupButtonClicked(sender: UIButton!) {
        if signupPasswordTextField.text != signupPasswordAgainTextField.text {
            print("달라")
            return
        }
        
        
        let resource = Resource<Basic>(url: URL.signupUrl)
        let body: [String: Any] = ["email": signupEmailTextField.text,"username": signupUsernameTextField.text, "password": signupPasswordTextField.text, "password_dup": signupPasswordAgainTextField.text]
        
        URLRequest.post(resource: resource, body: body)
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { code in
                if code == 200 {
                    self.navigationController?.popViewController(animated: true)

                } else {
                    let alert = UIAlertController(title: "회원가입 오류".localized, message: "아이디가 이미 존재하거나, 비밀번호가 일치하지 않습니다".localized, preferredStyle: .alert)
                    let cancel = UIAlertAction(title: "확인".localized, style: .cancel, handler: nil)
                    
                    alert.addAction(cancel)
                    self.present(alert, animated: true)
                }
            }).disposed(by: disposeBag)
        
    }
}
