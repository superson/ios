//
//  SettingViewController.swift
//  Every-Review
//
//  Created by 조수형 on 18/07/2019.
//  Copyright © 2019 suhyeongCho. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class SettingTableViewController: UITableViewController {
    
    let disposeBag = DisposeBag()
    
    @IBOutlet var accountCell: UITableViewCell!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    

    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
     
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    
}
extension SettingTableViewController {
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 1 {
            return 0
        } else {
            return 54
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "SettingCell", for: indexPath) as? SettingTableViewCell else {
//             assert(false)
            return UITableViewCell()
         }
        
        switch indexPath.row {
        case 0:
            cell.icon.image = UIImage(named: "ic_setting_profile")
            if UserDefaults.standard.bool(forKey: "UserLogin") == false {
                cell.titleLabel.text = "로그인".localized
            } else {
                cell.titleLabel.text = "로그아웃".localized
            }
        case 1:
            cell.icon.image = UIImage(named: "ic_setting_review")
            cell.titleLabel.text = "내가 쓴 리뷰"
        case 2:
            cell.icon.image = UIImage(named: "ic_setting_language")
            cell.titleLabel.text = "언어 설정".localized
        default:
            cell.icon.image = nil
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        if indexPath.row == 0 {
            
            if UserDefaults.standard.bool(forKey: "UserLogin") == false {
                guard let vc = storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as? LogInViewController else {
//                    assert(false)
                    return
                }
                
                vc.loginObserver
                    .subscribe(onNext: { result in
                        if result {
                            DispatchQueue.main.async {
                                tableView.reloadData()
                            }
                        }
                    }).disposed(by: disposeBag)
                navigationController?.pushViewController(vc, animated: true)
            } else {
                let resource = Resource<Basic>(url: URL.logoutUrl)

                URLRequest.post(resource: resource, body: nil, operation: .Logout)
                .subscribe(onNext: { code in
                    if code == 200 {
                        UserDefaults.standard.set(false, forKey: "UserLogin")
                        UserDefaults.standard.set(nil, forKey: "UserName")
                        
                        DispatchQueue.main.async {
                            tableView.reloadData()
                        }
                    } else {
                        print("실패")
                    }
                }).disposed(by: disposeBag)
            }

            
            
        } else if indexPath.row == 2 {
            guard let vc = storyboard?.instantiateViewController(withIdentifier: "LanguageTableViewController") as? LanguageTableViewController else {
//                assert(false)
                return
            }
            
            navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    

}
