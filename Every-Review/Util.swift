//
//  Util.swift
//  Every-Review
//
//  Created by 조수형 on 13/07/2019.
//  Copyright © 2019 suhyeongCho. All rights reserved.
//

import UIKit


extension UICollectionViewFlowLayout {
    func setDefaultLayout() {
        scrollDirection = .horizontal
        itemSize = CGSize(width: 200, height: 200)
        minimumLineSpacing = 10
        minimumInteritemSpacing = 10
    }
}
