//
//  Edit.swift
//  Every-Review
//
//  Created by 조수형 on 2019/11/20.
//  Copyright © 2019 suhyeongCho. All rights reserved.
//

import Foundation

struct Edit: Decodable {
    let title: String
    let edit_text: String
}
