//
//  Review.swift
//  Every-Review
//
//  Created by 조수형 on 2019/10/15.
//  Copyright © 2019 suhyeongCho. All rights reserved.
//

import Foundation

struct ReviewList: Decodable {
    let reviews: [Review]
}

struct Review: Decodable {
    let id: String
    let nickname: String
    let rating: Double
    let date: Double
    let content: String
    let like_cnt: Int
    let ref_name: String?
    let ref_url: String?
    let is_liked: Bool
}

