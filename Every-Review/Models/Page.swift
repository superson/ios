//
//  File.swift
//  Every-Review
//
//  Created by 조수형 on 2019/11/12.
//  Copyright © 2019 suhyeongCho. All rights reserved.
//

import Foundation

struct Page :Decodable {
    let results: [String]
}
