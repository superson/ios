//
//  Basic.swift
//  Every-Review
//
//  Created by 조수형 on 2019/10/15.
//  Copyright © 2019 suhyeongCho. All rights reserved.
//

import Foundation

struct Basic: Decodable {
    let result: Int
}
