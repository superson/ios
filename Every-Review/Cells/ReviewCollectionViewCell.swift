//
//  ReviewCollectionViewCell.swift
//  Every-Review
//
//  Created by 조수형 on 2019/10/12.
//  Copyright © 2019 suhyeongCho. All rights reserved.
//

import UIKit
import RxSwift

class ReviewCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var nicknameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var referenceLabel: UILabel!
    @IBOutlet weak var reviewLabel: UILabel!
    
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var likeCountLabel: UILabel!
    
    @IBOutlet var ratingImage1: UIImageView!
    @IBOutlet var ratingImage2: UIImageView!
    @IBOutlet var ratingImage3: UIImageView!
    @IBOutlet var ratingImage4: UIImageView!
    @IBOutlet var ratingImage5: UIImageView!
    
    
    var isLiked: Bool = false
    
    var reviewTitle: String!
    var reviewId: String!
    
    var referenceUrl: URL!
    
    let disposeBag = DisposeBag()

    
    @IBAction func likeButtonClicked(sender: UIButton!) {

        
        let resource = Resource<Basic>(url: URL.likeURL(id: reviewId))
        if isLiked {
            likeButton.setImage(UIImage(named: "ic_like_off"), for: .normal)
            isLiked = false
            

            
            let body: [String: Any] = ["id": reviewId!, "is_liked": false]
            URLRequest.post(resource: resource, body: body)
                .subscribe(onNext: { code in
                    print(code)
                }).disposed(by: disposeBag)
            
            var likeCount = Int(likeCountLabel.text!)!
            
            likeCount -= 1
            
            likeCountLabel.text = "\(likeCount)"
            
            
        } else {
            likeButton.setImage(UIImage(named: "ic_like_on"), for: .normal)
            isLiked = true
        
            
            let body: [String: Any] = ["id": reviewId!, "is_liked": true]
            URLRequest.post(resource: resource, body: body)
                .subscribe(onNext: {code in
                    print(code)
                }).disposed(by: disposeBag)
            
            var likeCount = Int(likeCountLabel.text!)!
            
            likeCount += 1
            
            likeCountLabel.text = "\(likeCount)"
        }
    }
}
