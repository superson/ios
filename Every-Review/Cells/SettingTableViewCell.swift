//
//  SettingTableViewCell.swift
//  Every-Review
//
//  Created by 조수형 on 2019/10/13.
//  Copyright © 2019 suhyeongCho. All rights reserved.
//

import UIKit

class SettingTableViewCell: UITableViewCell {
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
}
