//
//  EditReviewViewController.swift
//  Every-Review
//
//  Created by 조수형 on 18/07/2019.
//  Copyright © 2019 suhyeongCho. All rights reserved.
//

import UIKit
import RxSwift

class EditReviewViewController: UIViewController {

    
    var reviewTitle: String!
    @IBOutlet var nicknameLabel: UILabel!
    @IBOutlet var contentTextView: UITextView!
    
    @IBOutlet var ratingImage1: UIImageView!
    @IBOutlet var ratingImage2: UIImageView!
    @IBOutlet var ratingImage3: UIImageView!
    @IBOutlet var ratingImage4: UIImageView!
    @IBOutlet var ratingImage5: UIImageView!

    let disposeBag = DisposeBag()

    
    @IBOutlet var ratingImageStackView: UIStackView!
    var ratingStep: Int = 0
    
    private var reviewSubject = PublishSubject<Int>()
    
    var reviewObservable : Observable<Int>{
        return reviewSubject.asObservable()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        nicknameLabel.text = UserDefaults.standard.string(forKey: "UserName")!
        
    }

    
    @IBAction func saveButtonClicked(sender: UIBarButtonItem!) {
        
        let resource = Resource<Basic>(url: URL.reviewUrl(title: reviewTitle))
        
        let rating = Double(ratingStep) / 2
        
        let body: [String: Any] = ["rating": "\(rating)", "content" : contentTextView.text]

        URLRequest.post(resource: resource, body: body)
            .subscribe(onNext: { code in
                if code == 200 {
                    self.reviewSubject.onNext(code)
                }
            }).disposed(by: disposeBag)
        
        navigationController?.popViewController(animated: true)
    }
    
    
    
    
    
    
    
    
    @IBAction func panGestureRecognizerChanged(sender: UIPanGestureRecognizer) {
        
        print(sender.velocity(in: ratingImageStackView).x)
        if sender.velocity(in: ratingImageStackView).x > 400 {
            
            switch ratingStep {
            case 0:
                ratingImage1.image = UIImage(named: "ic_star_edit_half")
                ratingStep += 1
            case 1:
                ratingImage1.image = UIImage(named: "ic_star_edit_full")
                ratingStep += 1
            case 2:
                ratingImage2.image = UIImage(named: "ic_star_edit_half")
                ratingStep += 1
            case 3:
                ratingImage2.image = UIImage(named: "ic_star_edit_full")
                ratingStep += 1
            case 4:
                ratingImage3.image = UIImage(named: "ic_star_edit_half")
                ratingStep += 1
            case 5:
                ratingImage3.image = UIImage(named: "ic_star_edit_full")
                ratingStep += 1
            case 6:
                ratingImage4.image = UIImage(named: "ic_star_edit_half")
                ratingStep += 1
            case 7:
                ratingImage4.image = UIImage(named: "ic_star_edit_full")
                ratingStep += 1
            case 8:
                ratingImage5.image = UIImage(named: "ic_star_edit_half")
                ratingStep += 1
            case 9:
                ratingImage5.image = UIImage(named: "ic_star_edit_full")
                ratingStep += 1
            default:
                print("default")
            }
            
            
        } else if sender.velocity(in: ratingImageStackView).x < -400 {
            switch ratingStep {
            case 10:
                ratingImage5.image = UIImage(named: "ic_star_edit_half")
                ratingStep -= 1
            case 9:
                ratingImage5.image = UIImage(named: "ic_star_edit_empty")
                ratingStep -= 1
            case 8:
                ratingImage4.image = UIImage(named: "ic_star_edit_half")
                ratingStep -= 1
            case 7:
                ratingImage4.image = UIImage(named: "ic_star_edit_empty")
                ratingStep -= 1
            case 6:
                ratingImage3.image = UIImage(named: "ic_star_edit_half")
                ratingStep -= 1
            case 5:
                ratingImage3.image = UIImage(named: "ic_star_edit_empty")
                ratingStep -= 1
            case 4:
                ratingImage2.image = UIImage(named: "ic_star_edit_half")
                ratingStep -= 1
            case 3:
                ratingImage2.image = UIImage(named: "ic_star_edit_empty")
                ratingStep -= 1
            case 2:
                ratingImage1.image = UIImage(named: "ic_star_edit_half")
                ratingStep -= 1
            case 1:
                ratingImage1.image = UIImage(named: "ic_star_edit_empty")
                ratingStep -= 1
            default:
                print("default")
            }
        }
    }

}

extension EditReviewViewController: UITextViewDelegate {

    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.tag == 0 {
            textView.textColor = .black
            textView.text = ""
            textView.tag = 1
        }
    }
    
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.textColor = UIColor(displayP3Red: 182/255, green: 187/255, blue: 196/255, alpha: 1.0)
            textView.text = "리뷰를 작성해주세요."
            textView.tag = 0
        }
        textView.resignFirstResponder()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
}
