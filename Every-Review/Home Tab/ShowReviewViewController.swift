//
//  ShowReviewViewController.swift
//  Every-Review
//
//  Created by 조수형 on 2019/11/05.
//  Copyright © 2019 suhyeongCho. All rights reserved.
//

import UIKit

class ShowReviewViewController: UIViewController {

    
    @IBOutlet var contentTextView: UITextView!
    @IBOutlet var nicknameLabel: UILabel!
    
    @IBOutlet var ratingImage1: UIImageView!
    @IBOutlet var ratingImage2: UIImageView!
    @IBOutlet var ratingImage3: UIImageView!
    @IBOutlet var ratingImage4: UIImageView!
    @IBOutlet var ratingImage5: UIImageView!
    

    var content: String!
    var nickname: String!
    var rating: Double!
    
    var reviewTitle: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = reviewTitle
        
        
        
        nicknameLabel.text = nickname
        contentTextView.text = content

        
        ratingImage1.image = UIImage(named: "ic_star_edit_full")
        ratingImage2.image = UIImage(named: "ic_star_edit_full")
        ratingImage3.image = UIImage(named: "ic_star_edit_full")
        ratingImage4.image = UIImage(named: "ic_star_edit_full")
        ratingImage5.image = UIImage(named: "ic_star_edit_full")
        
        if rating < 5.0 {
            ratingImage5.image = UIImage(named: "ic_star_edit_half")
        }
        if rating < 4.5 {
            ratingImage5.image = UIImage(named: "ic_star_edit_empty")
        }
        if rating < 4.0 {
            ratingImage4.image = UIImage(named: "ic_star_edit_half")
        }
        if rating < 3.5 {
            ratingImage4.image = UIImage(named: "ic_star_edit_empty")
        }
        if rating < 3.0 {
            ratingImage3.image = UIImage(named: "ic_star_edit_half")
        }
        if rating < 2.5 {
            ratingImage3.image = UIImage(named: "ic_star_edit_empty")
        }
        if rating < 2.0 {
            ratingImage2.image = UIImage(named: "ic_star_edit_half")
        }
        if rating < 1.5 {
            ratingImage2.image = UIImage(named: "ic_star_edit_empty")
        }
        if rating < 1.0 {
            ratingImage1.image = UIImage(named: "ic_star_edit_half")
        }
        if rating < 0.5 {
            ratingImage1.image = UIImage(named: "ic_star_edit_empty")
        }
    }
}
