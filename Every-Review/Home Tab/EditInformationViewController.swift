//
//  EditInformationViewController.swift
//  Every-Review
//
//  Created by 조수형 on 18/07/2019.
//  Copyright © 2019 suhyeongCho. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class EditInformationViewController: UIViewController {
    
    let disposeBag = DisposeBag()
    
    var reviewTitle: String!
    @IBOutlet var contentView: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = reviewTitle
        
        let resource = Resource<Edit>(url: URL.editInfoUrl(title: reviewTitle))
        
        URLRequest.load(resource: resource)
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { result in
                self.contentView.text = result.edit_text
            }).disposed(by: disposeBag)
    }

    @IBAction func saveButtonClicked(sender: UIBarButtonItem!) {
        
        let resource = Resource<Basic>(url: URL.editInfoUrl(title: reviewTitle))
        
        let body: [String: Any] = ["title": reviewTitle, "edit_text": contentView.text]
        
        URLRequest.post(resource: resource, body: body)
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { code in
                if code == 200 {
                    self.navigationController?.popViewController(animated: true)
                }
            }).disposed(by: disposeBag)
        
    }
}
