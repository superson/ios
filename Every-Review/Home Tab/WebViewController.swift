//
//  WebViewController.swift
//  Every-Review
//
//  Created by 조수형 on 08/07/2019.
//  Copyright © 2019 suhyeongCho. All rights reserved.
//

import UIKit
import WebKit
import RxSwift

class WebViewController: UIViewController {
    
    @IBOutlet var spinner: UIActivityIndicatorView!
    
    
    @IBOutlet weak var contentsView: UIView!
    @IBOutlet weak var webview: WKWebView!
    
    @IBOutlet weak var sortingReviewButton: UIButton!
    
    @IBOutlet weak var reviewCollectionView: UICollectionView!
    
    @IBOutlet var reviewHeaderView: UIView!

    @IBOutlet var contentsHeight: NSLayoutConstraint!
    @IBOutlet var collectionViewHeight: NSLayoutConstraint!
    
    
    var reviewTitle: String!
    
    let disposeBag = DisposeBag()
    
    var index = 0
    
    private var reviews = [Review]()
    
    override func loadView() {
        super.loadView()
        
        let contentController = WKUserContentController()
        let webConfiguration = WKWebViewConfiguration()
        
        
        webConfiguration.userContentController = contentController
        webview = WKWebView(frame: self.view.frame, configuration: webConfiguration)

        webview.scrollView.bounces = false
        webview.scrollView.isScrollEnabled = false

        contentsView.addSubview(webview)
        
        
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = reviewTitle

        
        webview.navigationDelegate = self
        webview.uiDelegate = self
        
        sortingReviewButton.borderAll()
        sortingReviewButton.borderRounded(radius: 18)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
                
        let url = URL.pageUrl(title: reviewTitle)
        var request = URLRequest(url: url)
        
        request.setValue("application-ios", forHTTPHeaderField: "X-ER-DEVICE")
        
        webview.load(request)
        
    }
    
    func callReviewAPI(index: Int = 0) {
        let resource = Resource<ReviewList>(url: URL.reviewUrl(title: reviewTitle, index: index))
        
        
        
        URLRequest.load(resource: resource)
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { result in
                print(index)
                if index == 0 {
                    self.reviews = result.reviews
                } else {
                    self.reviews += result.reviews
                }

                
                let height = CGFloat(self.reviews.count * (228 + 16) + 8)
                self.collectionViewHeight.constant = height
                
                self.reviewCollectionView.reloadData()
                
            }).disposed(by: disposeBag)
    }
    
    @IBAction func sortingReviewButtonClicked(sender: UIButton!) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
        
        let action1 = UIAlertAction(title: "최신순", style: .default) { _ in
            self.sortingReviewButton.setTitle("최신순", for: .normal)
        }
        let action2 = UIAlertAction(title: "추천순", style: .default) { _ in
            self.sortingReviewButton.setTitle("추천순", for: .normal)
        }
        let action3 = UIAlertAction(title: "별점순", style: .default) { _ in
            self.sortingReviewButton.setTitle("별점순", for: .normal)
        }
        let cancel = UIAlertAction(title: "취소", style: .destructive, handler: nil)
        
        alert.addAction(action1)
        alert.addAction(action2)
        alert.addAction(action3)
        alert.addAction(cancel)
        

        alert.view.borderRounded(radius: 5)
        alert.view.tintColor = .black
        alert.view.backgroundColor = .white
        
        
        present(alert, animated: true, completion: nil)

    }

    @IBAction func EditButtonClicked(_ sender: UIBarButtonItem!) {
        
        if UserDefaults.standard.bool(forKey: "UserLogin") {
        
            
            let alert = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
            
            let editReview = UIAlertAction(title: "리뷰 작성".localized, style: .default) { _ in
                guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "EditReviewViewController") as? EditReviewViewController else {
    //                assert(false)
                    return
                }
                vc.reviewTitle = self.reviewTitle

                vc.reviewObservable
                    .subscribe(onNext: { code in
                        if code == 200 {
                            print("Edit Review is Finish")
                            self.callReviewAPI()

                        
                        }
                    }).disposed(by: self.disposeBag)
                
                self.navigationController?.pushViewController(vc, animated: true)
            }
            let editInfo = UIAlertAction(title: "정보 수정".localized, style: .default) { _ in
                guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "EditInformationViewController") as? EditInformationViewController else {
                //                assert(false)
                                return
                }
                
                vc.reviewTitle = self.reviewTitle
                
                self.navigationController?.pushViewController(vc, animated: true)


            }
            let cancel = UIAlertAction(title: "취소".localized, style: .cancel, handler: nil)
            
            alert.addAction(editReview)
            alert.addAction(editInfo)
            
            alert.addAction(cancel)
            
            alert.view.borderRounded(radius: 5)
            alert.view.tintColor = .black
            alert.view.backgroundColor = .white
            
            present(alert, animated: true)
            
        } else {
            let alert = loginRequiredAlert()
            
            present(alert, animated: true)
        }
    }
    
    func loginRequiredAlert() -> UIAlertController {
        let alert = UIAlertController(title: "안내".localized, message: "해당 기능을 사용하기 위해서 로그인이 필요합니다.\n로그인 하시겠습니까?".localized, preferredStyle: .alert)
        
        let cancel = UIAlertAction(title: "취소".localized, style: .cancel)
        
        let goLoginView = UIAlertAction(title: "로그인".localized, style: .default) { _ in
            guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as? LogInViewController else {
//              assert(false)
                return
            }
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        alert.addAction(cancel)
        alert.addAction(goLoginView)

        alert.view.borderRounded(radius: 5)
        alert.view.tintColor = .black
        alert.view.backgroundColor = .white
        
        return alert
    }
    
    
}


extension WebViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return reviews.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ReviewCell", for: indexPath) as? ReviewCollectionViewCell else {
//            assert(false)
            return UICollectionViewCell()
        }
        
        cell.makeCardViewCell()
        
        
        let unixDate = reviews[indexPath.row].date
        let date = Date(timeIntervalSince1970: unixDate)
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT") //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        
        cell.reviewId = reviews[indexPath.row].id
        cell.dateLabel.text = dateFormatter.string(from: date)
        cell.nicknameLabel.text = reviews[indexPath.row].nickname
        if let ref = reviews[indexPath.row].ref_name {
            cell.referenceLabel.text = "출처 : \(ref)"
            cell.referenceUrl = URL(string: reviews[indexPath.row].ref_url!)

        } else {
            cell.referenceLabel.text = ""
        }
        
        cell.reviewLabel.text = reviews[indexPath.row].content
        
        cell.likeCountLabel.text = "\(reviews[indexPath.row].like_cnt)"
        
        
        let rating = reviews[indexPath.row].rating
        
        cell.ratingImage1.image = UIImage(named: "ic_star_edit_full")
        cell.ratingImage2.image = UIImage(named: "ic_star_edit_full")
        cell.ratingImage3.image = UIImage(named: "ic_star_edit_full")
        cell.ratingImage4.image = UIImage(named: "ic_star_edit_full")
        cell.ratingImage5.image = UIImage(named: "ic_star_edit_full")

        
        if rating < 5.0 {
            cell.ratingImage5.image = UIImage(named: "ic_star_edit_half")
        }
        if rating < 4.5 {
            cell.ratingImage5.image = UIImage(named: "ic_star_edit_empty")
        }
        if rating < 4.0 {
            cell.ratingImage4.image = UIImage(named: "ic_star_edit_half")
        }
        if rating < 3.5 {
            cell.ratingImage4.image = UIImage(named: "ic_star_edit_empty")
        }
        if rating < 3.0 {
            cell.ratingImage3.image = UIImage(named: "ic_star_edit_half")
        }
        if rating < 2.5 {
            cell.ratingImage3.image = UIImage(named: "ic_star_edit_empty")
        }
        if rating < 2.0 {
            cell.ratingImage2.image = UIImage(named: "ic_star_edit_half")
        }
        if rating < 1.5 {
            cell.ratingImage2.image = UIImage(named: "ic_star_edit_empty")
        }
        if rating < 1.0 {
            cell.ratingImage1.image = UIImage(named: "ic_star_edit_half")
        }
        if rating < 0.5 {
            cell.ratingImage1.image = UIImage(named: "ic_star_edit_empty")
        }
        
        
        
        
        
        cell.reviewTitle = reviewTitle
        
        if reviews[indexPath.row].is_liked {
            cell.likeButton.setImage(UIImage(named: "ic_like_on"), for: .normal)
            cell.isLiked = true
        } else {
            cell.likeButton.setImage(UIImage(named: "ic_like_off"), for: .normal)
            cell.isLiked = false
        }

        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 335, height: 228)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: false)
        
        
        
        if let ref = reviews[indexPath.row].ref_url {
            guard let vc = storyboard?.instantiateViewController(withIdentifier: "ReviewReferenceViewController") as? ReviewReferenceViewController else {
//                assert(false)
                return
            }
            
            vc.reviewTitle = reviewTitle
            
            vc.referenceUrl = ref
            
            navigationController?.pushViewController(vc, animated: true)
            
            
        } else {
            guard let vc = storyboard?.instantiateViewController(withIdentifier: "ShowReviewViewController") as? ShowReviewViewController else {
//                assert(false)
                return
            }

            
            vc.reviewTitle = reviewTitle

            vc.nickname = reviews[indexPath.row].nickname
            vc.content = reviews[indexPath.row].content
            vc.rating = reviews[indexPath.row].rating
            
            navigationController?.pushViewController(vc, animated: true)
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        print(indexPath.row)
        if indexPath.row + 1 == reviews.count {
            index += 1
            callReviewAPI(index: index)
        }
    }
    
    
    

}

extension WebViewController: WKNavigationDelegate, WKUIDelegate, WKScriptMessageHandler {
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {}
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("finish loaded \(webView.url!)")
        
        webView.evaluateJavaScript("document.documentElement.scrollHeight") { completion, error in
            
            
            if let error = error {
                print(error)
                return
            }

            let height = completion as! CGFloat
            let width = self.view.frame.width
            print(height)
            
            
            webView.frame.size = CGSize(width: width, height: height)
                        
            self.contentsHeight.constant = height


            self.callReviewAPI()
            
        }
        spinner.stopAnimating()
        reviewCollectionView.isHidden = false
        reviewHeaderView.isHidden = false
        
    }
}

