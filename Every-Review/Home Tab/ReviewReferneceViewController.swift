//
//  ReviewReferneceViewController.swift
//  Every-Review
//
//  Created by 조수형 on 2019/11/05.
//  Copyright © 2019 suhyeongCho. All rights reserved.
//

import UIKit
import WebKit

class ReviewReferenceViewController: UIViewController {
    @IBOutlet var referenceWebView: WKWebView!
    
    var reviewTitle: String!
    var referenceUrl: String!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = reviewTitle
        
        let url = URL(string: referenceUrl)!
        let request = URLRequest(url: url)
        
        referenceWebView.load(request)
        
    }
}
