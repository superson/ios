//
//  HomeViewController.swift
//  Every-Review
//
//  Created by 조수형 on 16/07/2019.
//  Copyright © 2019 suhyeongCho. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class SearchViewController: UIViewController {
    
    @IBOutlet weak var searchHStackView: UIStackView!
    @IBOutlet weak var searchingTextField: UITextField!
    @IBOutlet weak var searchButton: UIButton!
    
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        searchButton.borderRounded(radius: 17)
        
//        UserDefaults.standard.set(false, forKey: "UserLogin")
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        view.endEditing(true)
    }

//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        guard let destination = segue.destination as? WebViewController else {
////            assert(false)
//            return
//        }
//
//        destination.reviewTitle = searchingTextField.text
//    }
    
    
    @IBAction func searchButtonClicked(_ sender: UIButton!) {
        let searchTitle = searchingTextField.text!
        
        if searchTitle == "" {
            return
        }
        
        let resource = Resource<Page>(url: URL.searchUrl(title: searchTitle))
        
        URLRequest.load(resource: resource)
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { result in
                if result.results.count == 0 {
                    let alert = UIAlertController(title: nil, message: "검색결과가 존재하지 않습니다".localized, preferredStyle: .alert)
                    let ok = UIAlertAction(title: "확인".localized, style: .default, handler: nil)
                    
                    alert.addAction(ok)
                    
                    self.present(alert, animated: true)
                } else if result.results.count == 1 && result.results[0] == searchTitle {
                    guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "WebViewController") as? WebViewController else {
                        return
                    }
                    vc.reviewTitle = searchTitle
                    self.navigationController?.pushViewController(vc, animated: true)
                } else {
                    guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "SearchListViewController") as? SearchListViewController else {
                        return
                    }
                    vc.searchList = result.results
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }).disposed(by: disposeBag)
    }
    
    
    
}




extension SearchViewController: UITextFieldDelegate {
    override func touchesBegan(_: Set<UITouch>, with _: UIEvent?) {
        view.endEditing(true)
    }

    func textFieldShouldReturn(_: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
}
