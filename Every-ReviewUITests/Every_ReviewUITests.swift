//
//  Every_ReviewUITests.swift
//  Every-ReviewUITests
//
//  Created by 조수형 on 04/07/2019.
//  Copyright © 2019 suhyeongCho. All rights reserved.
//

import XCTest

class Every_ReviewUITests: XCTestCase {
    var app: XCUIApplication!
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        app = XCUIApplication()
        app.launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        app.tabBars.buttons["홈"].tap()
        app.buttons["검색"].tap()
        sleep(1)

        app.buttons["edit"].tap()
        sleep(1)
        app.sheets.buttons["정보 수정"].tap()
        sleep(1)
        app.navigationBars.buttons["토이스토리4"].tap()
        sleep(1)

        app.buttons["edit"].tap()
        sleep(1)
        app.sheets.buttons["정보 수정"].tap()
        sleep(1)
        app.navigationBars.buttons["수정"].tap()
        sleep(1)

        app.buttons["edit"].tap()
        sleep(1)
        app.sheets.buttons["리뷰 작성"].tap()
        sleep(1)
        app.navigationBars.buttons["토이스토리4"].tap()
        sleep(1)

        app.buttons["edit"].tap()
        sleep(1)
        app.sheets.buttons["리뷰 작성"].tap()
        sleep(1)
        app.navigationBars.buttons["작성"].tap()
        sleep(1)

        app.buttons["edit"].tap()
        sleep(1)
        app.sheets.buttons["취소"].tap()
        sleep(1)
        app.navigationBars.buttons["Search"].tap()
        sleep(1)

        app.tabBars.buttons["카테고리"].tap()
        sleep(1)

        app.tabBars.buttons["설정"].tap()
        sleep(1)

        app.tables.staticTexts["로그인"].tap()
        sleep(1)
        app.navigationBars.buttons["설정"].tap()
        sleep(1)

        app.tables.staticTexts["언어"].tap()
        sleep(1)
        app.tables.staticTexts["한국어"].tap()
        sleep(1)
        app.navigationBars.buttons["저장"].tap()
        sleep(1)
    }
}
